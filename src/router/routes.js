
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/achados-perdidos', component: () => import('pages/AchadosPerdidos.vue') },
      { path: '/favoritos', component: () => import('pages/Favoritos.vue') },
      { path: '/inserir-pet', component: () => import('pages/InserirPet.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
